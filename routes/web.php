<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'MainController@index') ->name('main'); 
Route::middleware(['auth'])->group(function () {  
    Route::get('/dashboard', 'DashboardChartController@index') ->name('dashboard'); 
    Route::get('/dashboard/{id}', 'DashboardChartController@show')->name('dashboard.show')->where('id', '[0-9]+');
    Route::get('preferences', 'PreferencesController@index') ->name('preferences'); 
    Route::post('preferences', 'PreferencesController@store') ->name('preferences.store');
});