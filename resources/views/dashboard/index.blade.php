@extends('layouts.dash')

@section('content')
<div class="container">
  <h1 class="display-4" >Dashboard</h1>
  <div class="d-flex flex-row">

    <div class=" w-50 p-2">

      <button class = "btn btn-primary" id="all">
        All
      </button>
      <button  class = "btn btn-primary" id="day">
        Day
      </button>
      <button  class = "btn btn-primary" id="week">
        Week
      </button>
      <button  class = "btn btn-primary" id="month">
        Month
      </button>
      
      <div class="card rounded" id="charts">
        <div class="card-body py-2 px-2" id="all_chart">
          <h2>All</h2>
          <div>{!! $dashChart['all']->container() !!}</div>
        </div>
        <div class=" card-body py-2 px-2"  id="day_chart">
          <h2>Day</h2>
          <div>{!! $dashChart['day']->container() !!}</div>
        </div>
        <div class="card-body py-2 px-2" id="week_chart">
          <h2>Week</h2>
          <div>{!! $dashChart['week']->container() !!}</div>
        </div>
        <div class="card-body py-2 px-2"  id="month_chart">
          <h2>Month</h2>
          <div>{!! $dashChart['month']->container() !!}</div>
        </div>
      </div>
      

    </div>
    <div class="list-group w-50 p-2">
      @foreach($currencies as $cur)
      <div>
        <a href="/dashboard/{{$cur['id']}}" class="list-group-item list-group-item-action @if($cur['id'] == $id) active @endif">
          {{ $cur['name'] }}
       
        </a>

      </div>
      @endforeach
    </div>
  </div>
</div>
@endsection
