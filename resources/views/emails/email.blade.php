@extends('layouts.email')

@section('content')
<h2>Currencies changes for a 24-hour period</h2>

<div >
  @for ($i = 0; $i < $count; $i++)
  <table>
    <tr>
      <th>Time</th>
      <th>{{$currencyNames[$i]->name}}</th> 
    </tr>
    @foreach($currencyHistory[$i] as $history)
    <tr>
      <td>{{$history->created_at}}</td>
      <td>{{$history->value}}</td>
    </tr>
    @endforeach
  </table>

  @endfor
</div>
@endsection
