@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="inner cover text-center">
        <div class="h1 inner mt-3 mb-3 font-weight-normal">{{ __('Enter Cryptocurrencies') }}</div>

        <div class="card-body">
          <form class="form-signin" method="POST" action="{{ route('preferences.store') }}">

            @if($errors->any())
              {!! implode('', $errors -> all('<div  class="alert alert-danger" role="alert"> :message</div>')) !!}
            @endif

            @csrf

            @for ($i = 1, $j = 0; $j < 5 && $i <= 5; $i++, $j++)

            <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Currency '.$i) }}</label>                     
              <div class="col-md-6">                          
                <select   name="currency[ {{ $j }} ]"   class="selectpicker form-control" title="Choose one of the following..." data-live-search="true">
                  <option disabled selected value> -- select an option -- </option>

                  @foreach ($currencies as $cur)

                  <option  value="{{ $cur -> id }}" {{ $preferences[$j] == $cur -> id ? 'selected = "selected"' : '' }}  >{{ $cur -> name}}</option>

                  @endforeach

                </select>

              </div>

            </div>

            @endfor

            <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                  {{ __('OK') }}
                </button>

              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
