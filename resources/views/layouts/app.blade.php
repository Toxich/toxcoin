    <!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">

      <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>{{ config('app.name', 'Laravel') }}</title>

      <script src="{{ asset('js/app.js') }}"></script>

      <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

      <title>Product example for Bootstrap</title>

      <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/product/">

      <!-- Bootstrap core CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <!-- Custom styles for this template -->
      <link href="{{ asset('css/product.css') }}" rel="stylesheet">
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     
      
   
    </head>

    <body>
      <header>
        <nav class="site-header sticky-top py-1">

          <div class="container d-flex flex-column flex-md-row justify-content-between">
            <a class="py-2 nav-link {{ (request()->is('main')) ? 'active' : '' }}" href="{{route('main')}}">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="d-block mx-auto"><circle cx="12" cy="12" r="10"></circle><line x1="14.31" y1="8" x2="20.05" y2="17.94"></line><line x1="9.69" y1="8" x2="21.17" y2="8"></line><line x1="7.38" y1="12" x2="13.12" y2="2.06"></line><line x1="9.69" y1="16" x2="3.95" y2="6.06"></line><line x1="14.31" y1="16" x2="2.83" y2="16"></line><line x1="16.62" y1="12" x2="10.88" y2="21.94"></line></svg>
            </a>

            <a class="py-2 d-none d-md-inline-block nav-link {{ (request()->is('main')) ? 'active' : '' }}" href="{{route('main')}}">Main</a>
            @if (Auth::check())
            <a class="py-2 d-none d-md-inline-block nav-link {{ (request()->is('dashboard')) ? 'active' : '' }}" href="{{route('dashboard')}}">Dashboard</a>
            
            @endif
            @guest
            <a class="nav-link {{ (request()->is('login')) ? 'active' : '' }}"
             href="{{ route('login') }}">{{ __('Login') }}</a>
             @if (Route::has('register'))
             <a class="nav-link {{ (request()->is('register')) ? 'active' : '' }}"
               href="{{ route('register') }}">{{ __('Sign up') }}</a>
               @endif
               @else

               <div class="dropdown">
                <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{Auth::user()->name }}
                  </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Settings</a>
                 <a class="dropdown-item py-2 d-none d-md-inline-block {{ (request()->is('preferences')) ? 'active' : '' }}" href="{{route('preferences')}}">Preferences</a>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
                  </a>
                </div>
              </div>
              

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
            @endguest

          </div>
        </nav>
      </header>  

      <main>
        @yield('content')
      </main>


      <footer class="mastfoot mt-auto">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="d-block mb-2"><circle cx="12" cy="12" r="10"></circle><line x1="14.31" y1="8" x2="20.05" y2="17.94"></line><line x1="9.69" y1="8" x2="21.17" y2="8"></line><line x1="7.38" y1="12" x2="13.12" y2="2.06"></line><line x1="9.69" y1="16" x2="3.95" y2="6.06"></line><line x1="14.31" y1="16" x2="2.83" y2="16"></line><line x1="16.62" y1="12" x2="10.88" y2="21.94"></line></svg>
              <small class="d-block mb-3 text-muted">&copy; 2019-2020</small>
            </div>

            <div class="inner">
              <p>Toxcoin by Anton Chepurenko. Backend 2019.</p>
            </div>

          </div>
        </div>
      </footer>


        <!-- Bootstrap core JavaScript
          ================================================== -->
          <!-- Placed at the end of the document so the pages load faster -->
         
          <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
      
      
          <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.6/holder.min.js"></script>
           <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
     
          <script>
            Holder.addTheme('thumb', {
              bg: '#55595c',
              fg: '#eceeef',
              text: 'Thumbnail'
            });
          </script>
        </body>
        </html>
