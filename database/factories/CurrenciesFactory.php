<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Currency;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

  $curl = curl_init();
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_URL, "https://min-api.cryptocompare.com/data/top/totalvolfull?limit=10&tsym=USD");
    $curlRequest = curl_exec($curl);
    $curlResult = json_decode($curlRequest,true);
   $currencies = [];
   foreach ($curlResult["Data"] as $key) {
      $currencies[] = $key ["CoinInfo"] ["Name"]; 
    }

$factory->define(\App\Currency::class, function (Faker $faker) use ($currencies) {

        return [
            'name' => $faker->unique()->randomElement($currencies),
        ];
    });


