<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\CurrencyHistory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\CurrencyHistory::class, function (Faker $faker) {
        return [
            'currency_id' => DB::table('currencies')->inRandomOrder()->value('id'),
            'created_at' => $faker->dateTimeBetween($startDate = '-1 day', $endDate = 'now'),
            'value' => rand(0,100)*100,
        ];
    });


