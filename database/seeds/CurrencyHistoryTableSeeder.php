<?php

use Illuminate\Database\Seeder;

class CurrencyHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CurrencyHistory::class, 20)->create();
    }
}
  