<?php

namespace App\Console\Commands;

use Auth;
use Illuminate\Console\Command;
use Mail;
use Carbon\Carbon;

use App\Mail\SendMail;
use App\Currency;
use App\CurrencyHistory;
use App\User;
use App\Preference;


class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::get();
        foreach ($users as $key) {
            $userId = $key->id;
            $preferences = Preference::select('currency_id')->where('user_id', $userId)->get();
            $currenciesName = Currency::find($preferences);
            foreach ($preferences as $key) {
                $currencyHistory[] = CurrencyHistory::where('currency_id', $key->currency_id)->where('created_at', '>=', Carbon::now()->subDay())->get();
            } 
            $count = count($currenciesName);
            $email = User::getEmail($userId);

            Mail::to($email)->send(new SendMail($count, $currenciesName, $currencyHistory));
        }
    }
}
