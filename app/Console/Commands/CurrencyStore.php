<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Currency;
use App\CurrencyHistory;

class CurrencyStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $curl = curl_init();
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       $currencies = Currency::get();

        foreach ($currencies as $cur) {
 
       curl_setopt($curl, CURLOPT_URL, "https://min-api.cryptocompare.com/data/price?fsym=".$cur->name."&tsyms=USD");

       $res = curl_exec($curl);
       $rr = json_decode($res,true);
       $value = $rr['USD'];

       CurrencyHistory::store($cur->id, $value);


        }
    }
}
