<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CurrencyHistory extends Model
{
    protected $table = 'currency_histories';
    protected $fillable = ['currency_id','value'];

    public static function store($currencyId, $value)
    {
          CurrencyHistory::create(['currency_id' => $currencyId, 'value' => $value]);
    }

    public static function getTime($currencyId){
      $currencyHistory['all'] = CurrencyHistory::get()->where('currency_id', $currencyId);
      $currencyHistory['day'] = CurrencyHistory::get()->where('currency_id', $currencyId)->where('created_at', '>=', Carbon::now()->subDay());
      $currencyHistory['week'] = CurrencyHistory::get()->where('currency_id', $currencyId)->where('created_at', '>=', Carbon::now()->subWeek());
      $currencyHistory['month'] = CurrencyHistory::get()->where('currency_id', $currencyId)->where('created_at', '>=', Carbon::now()->subMonth());

      foreach ($currencyHistory['all'] as $key) {
        $time['all'][] = $key['created_at']->format('d M H:i');
      }

      foreach ($currencyHistory['day'] as $key) {
        $time['day'][] = $key['created_at']->format('d M H:i');
      }

      foreach ($currencyHistory['week'] as $key) {
        $time['week'][] = $key['created_at']->format('d M H:i');
      }
      foreach ($currencyHistory['month'] as $key) {
        $time['month'][] = $key['created_at']->format('d M H:i');
      }

      return $time;
    }

    public static function getValue($currencyId){
      $currencyHistory['all'] = CurrencyHistory::get()->where('currency_id', $currencyId);
      $currencyHistory['day'] = CurrencyHistory::get()->where('currency_id', $currencyId)->where('created_at', '>=', Carbon::now()->subDay());
      $currencyHistory['week'] = CurrencyHistory::get()->where('currency_id', $currencyId)->where('created_at', '>=', Carbon::now()->subWeek());
      $currencyHistory['month'] = CurrencyHistory::get()->where('currency_id', $currencyId)->where('created_at', '>=', Carbon::now()->subMonth());

      foreach ($currencyHistory['all'] as $key) {
        $value['all'][] = $key['value'];
      }

      foreach ($currencyHistory['day'] as $key) {
        $value['day'][] = $key['value'];
      }

      foreach ($currencyHistory['week'] as $key) {
        $value['week'][] = $key['value'];
      }
      foreach ($currencyHistory['month'] as $key) {
        $value['month'][] = $key['value'];
      }

      return $value;
    }
}
