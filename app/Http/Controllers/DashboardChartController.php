<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\DashboardChart;
use App\CurrencyHistory;
use App\Preference;
use App\Currency;
use Auth;



class DashboardChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $userId = Auth::id();
        $preferences = Preference::select('currency_id')->where('user_id', $userId)->get();
        $currenciesName = Currency::find($preferences);

        $activeCurrency = $currenciesName->first();
        $id = $activeCurrency['id'];

        $time = CurrencyHistory::getTime($id);

        $values =  CurrencyHistory::getValue($id);  
       
        $activeCurrency = $currenciesName->firstWhere('id', $id);


        $dashChart['all'] = new DashboardChart;
        $dashChart['all']->labels($time['all']);
        $dashChart['all']->dataset($activeCurrency['name'], 'line', $values['all'] )
                         ->color("rgb(52, 128, 235)")
                         ->fill(false)
                         ->linetension(0.0);

        $dashChart['day'] = new DashboardChart;
        $dashChart['day']->labels($time['day']);
        $dashChart['day']->dataset($activeCurrency['name'], 'line', $values['day'] )
                         ->color("rgb(52, 128, 235)")
                         ->linetension(0.0);

        $dashChart['week'] = new DashboardChart;
        $dashChart['week']->labels($time['week']);
        $dashChart['week']->dataset($activeCurrency['name'], 'line', $values['week'] )
                          ->color("rgb(52, 128, 235)")
                          ->linetension(0.0);

        $dashChart['month'] = new DashboardChart;
        $dashChart['month']->labels($time['month']);
        $dashChart['month']->dataset($activeCurrency['name'], 'line', $values['month'] )
                           ->color("rgb(52, 128, 235)")
                           ->linetension(0.0);

        return view('dashboard.index', [ 
                                        'dashChart' => $dashChart,
                                        'currencies' => $currenciesName,
                                        'id' => $id
                                        ] );    
      
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $userId = Auth::id();
        $preferences = Preference::select('currency_id')->where('user_id', $userId)->get();
        $currenciesName = Currency::find($preferences);

        $time = CurrencyHistory::getTime($id);

        $values =  CurrencyHistory::getValue($id);  
       
        $activeCurrency = $currenciesName->firstWhere('id', $id);


        $dashChart['all'] = new DashboardChart;
        $dashChart['all']->labels($time['all']);
        $dashChart['all']->dataset($activeCurrency['name'], 'line', $values['all'] )
                         ->color("rgb(52, 128, 235)")
                         ->linetension(0.0);

        $dashChart['day'] = new DashboardChart;
        $dashChart['day']->labels($time['day']);
        $dashChart['day']->dataset($activeCurrency['name'], 'line', $values['day'] )
                        ->color("rgb(52, 128, 235)")
                         ->linetension(0.0);

        $dashChart['week'] = new DashboardChart;
        $dashChart['week']->labels($time['week']);
        $dashChart['week']->dataset($activeCurrency['name'], 'line', $values['week'] )
                          ->color("rgb(52, 128, 235)")
                          ->linetension(0.0);

        $dashChart['month'] = new DashboardChart;
        $dashChart['month']->labels($time['month']);
        $dashChart['month']->dataset($activeCurrency['name'], 'line', $values['month'] )
                           ->color("rgb(52, 128, 235)")
                           ->linetension(0.0);

        return view('dashboard.index', [ 
                                        'dashChart' => $dashChart,
                                        'currencies' => $currenciesName,
                                        'id' => $id
                                        ] );    
    }

  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
