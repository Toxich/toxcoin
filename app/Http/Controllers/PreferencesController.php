<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Preference;
use App\Currency;
use Validator;
use Session;

class PreferencesController extends Controller
{
 public function index()
 {
  $userId = Auth::id();
  $preferences = Preference::select('currency_id')->where('user_id', $userId)->get();

  $currencies = Currency::get();

  $preferencesResult = [];
  foreach ($preferences as $key) {
    $preferencesResult[] = $key -> currency_id; 
  }
  $count = count($preferencesResult);
   for($i = 0; $i < 5 - $count; $i++){
    $preferencesResult[] = 0;
  }
  
  return view('preferences.index', ['preferences' => $preferencesResult, 'currencies' => $currencies] ) ;
}


public function store(Request $request)
{
  $validator = Validator::make($request->all(), [
    'currency.*' => 'required|exists:currencies,id|distinct',
  ]);
  if ($validator->fails()) {
    Session::flash('error', $validator->messages()->first());
    return redirect()->back()->withInput()->withErrors($validator);
  }

  Preference::deleteAll(Auth::id());

  foreach ($request['currency'] as $key) {
    Preference::store(Auth::id(), $key);
  }
  return redirect()->route('dashboard');
}



}
