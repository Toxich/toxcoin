<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    protected $table = 'preferences';
    protected $fillable = ['user_id','currency_id'];

     public static function store($userId, $currencyId)
    {
          $pref = Preference::create(['user_id' => $userId, 'currency_id' => $currencyId]);
    }

    public static function deleteAll($userId){
        $rows = Preference::get()->where('user_id', $userId);
        foreach ($rows as $key) {
          Preference::find($key->id)->delete();
        }
    }


}
