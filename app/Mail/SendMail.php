<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $count;
    public $currencyHistory;
    public $currencyNames;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($count, $currencyNames,$currencyHistory)
    {
        $this->count = $count;
        $this->currencyNames = $currencyNames;
        $this->currencyHistory = $currencyHistory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('toxcoin@example.com')->view('emails.email', ['count' => $this->count, 'currencyNames' => $this->currencyNames, 'currencyHistory' => $this->currencyHistory ]);
    }
}
